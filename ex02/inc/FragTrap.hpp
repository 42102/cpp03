
#ifndef FRAGTRAP_HPP_
#define FRAGTRAP_HPP_

#include <iostream>
#include "./ClapTrap.hpp"

class FragTrap : public ClapTrap
{
	public:
        
        static const unsigned int hit_points_ft = 100;
        static const unsigned int energy_points_ft = 100;
        static const unsigned int attack_damage_ft = 30;

		//Functions to make ex01
		FragTrap(void);
		FragTrap(std::string name);
		FragTrap(FragTrap const &ft);
		~FragTrap();

        //Methods
		void highFiveGuys(void) const;

        //Operators
		const FragTrap& operator = (FragTrap const &ft);
};

#endif
