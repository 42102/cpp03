
#include "../inc/FragTrap.hpp"

int main(void)
{
    FragTrap c("Juan");
    FragTrap d("Pedro");
   	FragTrap e(d);
	FragTrap f;
	
	f = c;
    
    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
    e.getInfo();
	std::cout<<std::endl;
   	f.getInfo();
	std::cout<<std::endl;
	
	c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());

	c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());
    
	c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());
    
	c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());
    
    d.beRepaired(15);
   
   	c.highFiveGuys();
	d.highFiveGuys();

    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;

    return 0;
}
