
#include "../inc/FragTrap.hpp"

FragTrap::FragTrap(void)
	: ClapTrap()

{
	std::cout<<"FragTrap default constructor"<<std::endl;
    this->_hit_points = FragTrap::hit_points_ft;
    this->_energy_points = FragTrap::energy_points_ft;
    this->_attack_damage = FragTrap::attack_damage_ft;
}

FragTrap::FragTrap(std::string name)
	: ClapTrap(name)
{
	std::cout<<"FragTrap contructor with 1 parameter <string>"<<std::endl;
    this->_hit_points = FragTrap::hit_points_ft;
    this->_energy_points = FragTrap::energy_points_ft;
    this->_attack_damage = FragTrap::attack_damage_ft;
}

FragTrap::FragTrap(FragTrap const &ft)
	: ClapTrap(ft)
{
	std::cout<<"FragTrap copy constructor"<<std::endl;
}

FragTrap::~FragTrap()
{
	std::cout<<"Destroying FragTrap = { ";
	this->getInfo();
	std::cout<<" } "<<std::endl;
}

void FragTrap::highFiveGuys(void) const
{
	std::cout<<"For sure bro high five!!!"<<std::endl;
}

const FragTrap& FragTrap::operator = (FragTrap const &ft)
{
	if(this != &ft)
	{
		ClapTrap::operator=(ft);	
	}

	return *this;
}
