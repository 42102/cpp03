#include "../inc/ClapTrap.hpp"

//Constructors and Destructors 


ClapTrap::ClapTrap()
    : _name("claptrap default name"), _hit_points(10),
    _energy_points(10), _attack_damage(0)
{
    std::cout<<"ClapTrap default constructor"<<std::endl;
}

ClapTrap::ClapTrap(std::string name)
    : _name(name), _hit_points(10), _energy_points(10),
    _attack_damage(0)
{
    std::cout<<"ClapTrap with 1 parameter<string>"<<std::endl;
}

ClapTrap::ClapTrap(ClapTrap const &ct)
{
	*this = ct;
    std::cout<<"ClapTrap copy constructor"<<std::endl;
}

ClapTrap::~ClapTrap()
{
    std::cout<<"Destroying ClapTrap = { ";
    this->getInfo();
    std::cout<<" }"<<std::endl;
}

//Aux functions ex00
std::string ClapTrap::getName(void) const
{
    return (this->_name);
}

int ClapTrap::getHitPoints(void) const
{
    return (this->_hit_points);
}


int ClapTrap::getEnergyPoints(void) const
{
    return (this->_energy_points);
}


int ClapTrap::getAttackDamage(void) const
{
    return (this->_attack_damage);
}

void ClapTrap::getInfo(void) const
{
    std::cout<<"Name: " << this->getName() << ", Hit Points: " << 
       this->getHitPoints() << ", Energy Points: " << this->getEnergyPoints() <<
      ", Attack Damage:  " << this->getAttackDamage() ;

}

//Functions ex00
void ClapTrap::attack(std::string const &target)
{

	if(this->_hit_points <= 0)
	{
		std::cout<<this->getName()<<" is died!"<<std::endl;
		return ;
	}
    if(this->getEnergyPoints() <= 0)
    {
        std::cout<<this->getName()<<" has not enough energy points to attack!"<<std::endl;
        return ;
    }

	std::cout<<"ClapTrap "<<this->getName()<<" attacks "<<target
		<<", causing "<<this->getAttackDamage()<<" points of damage!"<<std::endl;

	this->_energy_points--;
}

void ClapTrap::takeDamage(unsigned int amount)
{

    if(this->_hit_points <= 0)
    {
	    std::cout<<this->getName()<<" is already died!"<<std::endl;
	    return ;
    }

    if(this->_hit_points >= amount)
        this->_hit_points -=  amount;
    else
        this->_hit_points = 0;
}

void ClapTrap::beRepaired(unsigned int amount)
{
    if(this->_hit_points <= 0)
    {
        std::cout<<this->getName()<<" is already died!"<<std::endl;
        return ;
    }
    if(this->getEnergyPoints() > 0)
    {
        this->_hit_points += amount;
        std::cout<<this->getName()<<" has recovered "<<amount<<" health points"<<std::endl;
        this->_energy_points--;
    }
    else
    {
        std::cout<<"You have not enough energy points to be repaired!"<<std::endl; 
    }
}

//Operators
const ClapTrap& ClapTrap::operator = (ClapTrap const &ct)
{
	if(this != &ct)
	{
		this->_name = ct._name;
		this->_hit_points = ct._hit_points;
		this->_energy_points = ct._energy_points;
		this->_attack_damage = ct._attack_damage;
	}

	return *this;
}
