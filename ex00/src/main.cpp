
#include "../inc/ClapTrap.hpp"

int main(void)
{
    ClapTrap c("Juan");
    ClapTrap d("Pedro");
	ClapTrap e(d);
	ClapTrap f;

    
    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
    e.getInfo(); 
    std::cout<<std::endl;

	c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());

    d.beRepaired(15);
	
	f = d;	
    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
	f.getInfo();
    std::cout<<std::endl;

    return 0;
}
