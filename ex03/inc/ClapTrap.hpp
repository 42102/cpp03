
#ifndef CLAPTRAP_HPP_
#define CLAPTRAP_HPP_

#include <iostream>

class ClapTrap {

    protected:

        std::string _name;
        unsigned int _hit_points;
        unsigned int _energy_points;
        unsigned int _attack_damage;

    public:

		//Constructors and Destructors
        ClapTrap();
        ClapTrap(std::string name);
		ClapTrap(ClapTrap const &ct);
        ~ClapTrap(); 

		//Aux functions ex00
        std::string getName(void) const;
        int getHitPoints(void) const;
        int getEnergyPoints(void) const;
        int getAttackDamage(void) const;
        void getInfo(void) const;
         
        //Functions ex00
        void attack(std::string const &target);
        void takeDamage(unsigned int amount);
        void beRepaired(unsigned int amount);
		//Operators
		const ClapTrap& operator= (ClapTrap const &ct);
};

#endif
