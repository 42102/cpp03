
#ifndef DIAMOND_TRAP_HPP_
#define DIAMOND_TRAP_HPP_
#include "./FragTrap.hpp"
#include "ScavTrap.hpp"

class DiamondTrap : /* virtual public Claptrap, */public ScavTrap, public FragTrap {
	
	private:
		std::string _name;
			
	public:
        //Constructors and Destructors
		DiamondTrap(void);
		DiamondTrap(std::string name);
		DiamondTrap(DiamondTrap const &d);
		~DiamondTrap();
        
        //Methods
        void whoAmI(void) const;
        void attack(std::string const &target);

		//Operators
		const DiamondTrap& operator= (DiamondTrap const &d);

};
#endif
