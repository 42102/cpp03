
#include "../inc/DiamondTrap.hpp"

//Constructors and Destructors
DiamondTrap::DiamondTrap(void)
	:ClapTrap("_clap_name")
{
	this->_name = "";
	this->_hit_points = FragTrap::hit_points_ft;
	this->_energy_points = ScavTrap::energy_points_st;
	this->_attack_damage = FragTrap::attack_damage_ft;

    std::cout<<"DiamondTrap default constructor"<<std::endl;
}

DiamondTrap::DiamondTrap(std::string name)
	: ClapTrap(name + "_clap_name")
{

	this->_name = name;
	this->_hit_points = FragTrap::hit_points_ft;
	this->_energy_points = ScavTrap::energy_points_st;
	this->_attack_damage = FragTrap::attack_damage_ft;

    std::cout<<"DiamondTrap constructor with 1 parameter<string>"<<std::endl;
}


DiamondTrap::DiamondTrap(DiamondTrap const &d)
	:ClapTrap(d), ScavTrap(d), FragTrap(d)
{
    *this = d;
	std::cout<<"DiamondTrap copy constructor"<<std::endl;
}

void DiamondTrap::attack(std::string const &target)
{
	this->ScavTrap::attack(target);
}

DiamondTrap::~DiamondTrap()
{
	std::cout<<"Destroying DiamondTrap object"<<std::endl;
}

//Methods
void DiamondTrap::whoAmI(void) const
{
    std::cout<<"Name of DiamondTrap class: "<<this->_name<<std::endl;
    std::cout<<"Name of ClapTrap class: "<<ClapTrap::_name<<std::endl;
}

//Operators
const DiamondTrap& DiamondTrap::operator= (DiamondTrap const &d)
{
	if(this != &d)
	{

		ClapTrap::operator=(d);
		FragTrap::operator=(d);
		ScavTrap::operator=(d);
		this->_name = d._name;
		this->_hit_points = d._hit_points;
		this->_energy_points = d._energy_points;
		this->_attack_damage = d._attack_damage;
	}

	return *this;
}
