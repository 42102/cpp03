
#include "../inc/ScavTrap.hpp"

//Constructors and Destructors
ScavTrap::ScavTrap(void)
	: ClapTrap()

{
    this->_hit_points = ScavTrap::hit_points_st;
    this->_energy_points = ScavTrap::energy_points_st;
    this->_attack_damage = ScavTrap::attack_damage_st;

	std::cout<<"ScavTrap default constructor"<<std::endl;
}

ScavTrap::ScavTrap(std::string name)
	: ClapTrap(name)
{

    this->_hit_points = ScavTrap::hit_points_st;
    this->_energy_points = ScavTrap::energy_points_st;
    this->_attack_damage = ScavTrap::attack_damage_st;
	
    std::cout<<"ScavTrap contructor with 1 parameter <string>"<<std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &st)
	: ClapTrap(st)
{
    *this = st;
	std::cout<<"ScavTrap copy constructor"<<std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout<<"Destroying ScavTrap = { ";
	this->getInfo();
	std::cout<<" } "<<std::endl;
}


//Methods
void ScavTrap::guardGate(void) const
{
	std::cout<<"ScavTrap "<<this->getName()<<" is now in gate keeper mode!"<<std::endl;
}

//Operators
const ScavTrap& ScavTrap::operator= (ScavTrap const &st)
{
	if(this != &st)
		ClapTrap::operator=(st);
	return *this;
}
