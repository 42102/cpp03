
#include "../inc/DiamondTrap.hpp"

int main(void)
{
	DiamondTrap c("Juan");
    DiamondTrap d("Pedro");
	DiamondTrap e(c);
    d.whoAmI();

    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
    e.getInfo(); 
    std::cout<<std::endl;
    
    c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());

    d.beRepaired(15);
   
   	c.guardGate();
	d.guardGate();

    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
    

    return 0;
}
