
#ifndef SCAVTRAP_HPP_
#define SCAVTRAP_HPP_

#include <iostream>
#include "./ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
    protected:
        static const unsigned int hit_points_st = 100;
        static const unsigned int energy_points_st = 50;
        static const unsigned int attack_damage_st = 20;
	public:
		//Functions to make ex01
		ScavTrap(void);
		ScavTrap(std::string name);
		ScavTrap(ScavTrap const &st);
        ~ScavTrap();
		
        void guardGate(void) const;

		//Operators
		const ScavTrap& operator= (ScavTrap const &st);
};

#endif
