
#include "../inc/ScavTrap.hpp"

int main(void)
{
    ScavTrap c("Juan");
    ScavTrap d("Pedro");
	ScavTrap e(c);
	ScavTrap f;
    
	f = c;
    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;
	e.getInfo();
    std::cout<<std::endl;
	f.getInfo();
    std::cout<<std::endl;

    c.attack(d.getName());
    d.takeDamage(c.getAttackDamage());

    d.beRepaired(15);
   
   	c.guardGate();
	d.guardGate();

    c.getInfo();
    std::cout<<std::endl;
    d.getInfo(); 
    std::cout<<std::endl;

    return 0;
}
